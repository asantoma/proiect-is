import numpy as np
import pandas as pd
import sklearn

from sklearn.ensemble import RandomForestRegressor

class Predictor:
    def __init__(self, train_data, test_data):
        self.regressor = RandomForestRegressor()
        self.train_data = train_data
        self.test_data = test_data

        # Remove movies that have already been rated
        self.__remove_rated_movies()

    def __fit(self):
        # Private method
        """
        Fits the regressor on the given training data
        which consists of movies that were rated by an user.
        """
        self.regressor.fit(
            X=self.train_data.drop(['title', 'rating'], axis=1),
            y=self.train_data['rating'])

    def __remove_rated_movies(self):
        # Private method
        """
        Removes the items already rated by the user from
        the test data object.
        """
        rated_movies_titles = self.train_data['title'].unique().tolist()
        self.test_data = self.test_data[
            ~self.test_data['title'].isin(rated_movies_titles)]


    def __predict(self):
        # Private method
        """
        Makes rating predictions for unrated movies.
        Sorts movies in descending order based on predicted rating.
        """
        self.test_data['rating'] = self.regressor.predict(
            X=self.test_data.drop(['title', 'rating'], axis=1))
        self.test_data.sort_values(by=['rating'], ascending=False, inplace=True)

    def make_recommendations(self, n=5):
        """
        Returns n best predicted movies as a Pandas DataFrame.
        """

        self.__fit()
        self.__predict()
        return pd.DataFrame(self.test_data.head(n))


# def main():
#     train_df = pd.read_csv('randoms.csv')
#     test_df = pd.read_csv('movie_database.csv')

#     p = Predictor(train_df, test_df)
#     result = p.make_recommendations(15)
#     result.to_csv('result.csv', index=False)

# if __name__ == "__main__":
#     main()
        
    