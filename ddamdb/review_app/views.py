from django.shortcuts import render
from django.http import HttpResponse
from .models import Post
from .models import Movie

# Create your views here.

def home(request):
    context={
        'movies': Movie.objects.all()
    }
    return render(request,'review_app/home.html',context)

def about(request):
    return render(request,'review_app/about.html',{'title':"Our Movies"})