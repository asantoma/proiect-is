"""
Wrapper module that manages the interaction between the Django app
and the .csv movie database.
"""

from review_app.models import Movie
import pandas as pd
import numpy as np

database_path = 'movie_database.csv'
db = pd.read_csv(database_path)
# Fetching list of columns as a global variable
# filtering columns related to genres
genre_columns_list = list(
                         filter(
                             lambda x : x.startswith('is_'), db.columns.tolist()))


def searchMovieByTitle(search_title):
    """
    Searches the database for a movie title.
    
    Returns row containing movie, if movie is found.
    Else, returns a list containing 5 movies whose titles starts with given parameter.
    """
    # Querying database
    searchResult = db[db.title.str.startswith(search_title)]
    
    # If query result has no rows, try to return a list
    # of titles beginning with string given as parameter
    # else, return the row containing searched movie
    if searchResult.shape[0] > 1:
        return sorted(searchResult.title.tolist())[:5]
    else:                
        return searchResult


def extractGenres(entry):
    """
    Extracts genres from an entry in the database.
    
    Returns them as a string containing comma-separated genres.
    """    
    entry_genre_list = list(filter(lambda column : (entry[column] != 0).bool(), genre_columns_list))
    entry_genre_list = list(map(lambda e : e[3:], entry_genre_list))
    return ', '.join(entry_genre_list)


def createMovieObject(search_title):
    """
    Creates  and returns a review_app.models.Movie object
    from movie with title given as parameter (if it exists).
    
    Returns None in case of failure.    
    """
    
    # Try searching for movie
    searchResult = searchMovieByTitle(search_title)
    
    # If search result is a list, no singular movie has been found
    # similarly, if search result is an empty dataframe, the search has failed
    # return None
    if (searchResult.shape[0] == 0):
        return None
    if isinstance(searchResult, list):
        return searchResult
    else:
        # else, start creating Movie object
        newMovie = Movie(title=search_title, year=2019-int(searchResult.age))
        # extract genres
        newMovie.genres = extractGenres(searchResult)        
        # fetch remaining attributes from db
        newMovie.runtime = int(searchResult.runtime)        
        return newMovie
