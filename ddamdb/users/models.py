from django.db import models
from django.contrib.auth.models import User
from review_app.models import Movie
import movie_db_util

class Profile(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    image = models.ImageField(default='default.jpg', upload_to='profile_pics')
    movies_list = []

    def __init__(self, *args, **kwargs):
        super(Profile, self).__init__(*args, **kwargs)

    def __str__(self):
        return f'{self.user.username} Profile'

    def addMovie(self, search_title):
        search_result = movie_db_util.createMovieObject(search_title)

        if isinstance(search_result, Movie):
            movies_list.append(search_result)

